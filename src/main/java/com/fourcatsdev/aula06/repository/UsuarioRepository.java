package com.fourcatsdev.aula06.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.aula06.modelo.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
